<?php

namespace WildShortcode\Filter;

use WildShortcode\Shortcode\HandlerInterface;
use Zend\Filter\AbstractFilter;

class Shortcode extends AbstractFilter
{
	protected $handlers = array();

	public function registerShortcode($code, HandlerInterface $handler)
	{
		$this->handlers[$code] = $handler;
		return $this;
	}

	public function unregisterShortcode($code)
	{
		if(isset($this->handlers[$code])) {
			unset($this->handlers[$code]);
		}
		return $this;
	}

	protected function getHandler($shortcode)
	{
		return $this->handlers[$shortcode];
	}

	public function filter($value)
	{
		$matches = null;

		if(!preg_match_all('/\{{2}(.*?)\}{2}/', $value, $matches)) return $value;

		foreach($matches[0] as $match) {
			$value = str_replace($match, $this->processMatch($match), $value);
		}

        $filteredValue = $value;
        return $filteredValue;
	}

	protected function processMatch($match)
	{
		$find = array('{{','}}');
        $replace = array('');
        $match = str_replace($find, $replace, $match);

		$shortcode = strtok($match, ' ');
        $arguments = array();

        if(preg_match_all('/ (.*?)="(.*?)"/', $match, $results)) {
        	$matches = $results[0];
        	$options = $results[1];
        	$values  = $results[2];

        	foreach($options as $key => $option) {
        		$arguments[$option] = $values[$key];
        	}
        }

        $handler = $this->getHandler($shortcode);

        ob_start();
        echo $handler->execute($arguments);
        $processed = ob_get_clean();
        return $processed;
	}
}