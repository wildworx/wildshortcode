<?php

namespace WildShortcode\Options;

use Zend\Stdlib\AbstractOptions;

class ModuleOptions extends AbstractOptions
{
	protected $__strictMode__ = false;

	protected $allow_view_filtering = true;

	public function allowViewFiltering($flag = null)
	{
		if(null !== $flag) {
			$this->allow_view_filtering = $flag ? true : false;
		}
		return $this->allow_view_filtering;
	}

	public function setViewFiltering($options)
	{
		if(array_key_exists('enabled', $options)) {
			$this->allowViewFiltering($options['enabled']);
		}
	}
}