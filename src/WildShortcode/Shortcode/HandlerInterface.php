<?php

namespace WildShortcode\Shortcode;

interface HandlerInterface
{
	public function execute($arguments);
}