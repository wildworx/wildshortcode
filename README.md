WildShortcode
=============
Provides shortcode functionality for ZF2 applications.

Features
--------
- Process entire view scripts through the shortcode filter (this is the default out-of-the-box behaviour, but it can easily be disabled).
- Easily add your own shortcodes by extending `Shortcode\HandlerInterface` and registering them via configuration.
- Register and de-register shortcodes at will. Useful if you want to selectively restrict shortcode access for some content -- just invoke a new `Shortcode\Filter` and add the shortcodes you need.

To Do
-----
- Review Filter\Shortcode as it was put together pretty hastily.
- Add config example. [COMPLETED]
- Write installation / usage instructions.