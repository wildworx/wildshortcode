<?php

namespace WildShortcode;

use Zend\Mvc\MvcEvent;

class Module
{
	public function onBootstrap(MvcEvent $e)
	{
		$sm = $e->getApplication()->getServiceManager();
		$options = $sm->get('wildshortcode_module_options');

		if($options->allowViewFiltering()) {
			$view = $sm->get('ViewRenderer');
			$view->getFilterChain()->attach($sm->get('WildShortcode\Filter\Shortcode'));
		}
	}

	public function getConfig()
	{
		return include __DIR__ . '/config/module.config.php';
	}

	public function getAutoloaderConfig()
	{
		return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
	}

	public function getServiceConfig()
	{
		return array(
			'factories' => array(
				'wildshortcode_module_options' => function ($sm) {
					$config = $sm->get('Config');
					return new Options\ModuleOptions(isset($config['wildshortcode']) ? $config['wildshortcode'] : array());
				},
				'WildShortcode\Filter\Shortcode' => function ($sm) {
					$config = $sm->get('Config');
					$filter = new Filter\Shortcode();
					if(isset($config['wildshortcode']['shortcodes'])) {
						foreach($config['wildshortcode']['shortcodes'] as $shortcode => $handler) {
							$filter->registerShortcode($shortcode, $sm->get($handler));
						}
					}
					return $filter;
				},
			)
		);
	}
}